package com.princess.sms.service.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.princess.sms.model.Voyage;


@RunWith(PowerMockRunner.class)
public class VoyageServiceImplTest {

  @InjectMocks
  VoyageServiceImpl voyageServiceImpl = new VoyageServiceImpl();

  @Mock
  RestTemplate restTemplate;

  @Mock
  String shipstarWSURL;


  @Before
  public void setUp() {
    // Whitebox.setInternalState(voyageServiceImpl, RestTemplate.class, restTemplate);
    Whitebox.setInternalState(voyageServiceImpl, String.class, shipstarWSURL);
  }

  @Test
  public void testFindCurrentVoyage() {
    StringBuilder wsUrl = new StringBuilder(shipstarWSURL).append("voyages/current");

    Voyage expectedVoyage = new Voyage();
    expectedVoyage.setVoyageNumber("123456");

    ResponseEntity<Voyage> response = new ResponseEntity<Voyage>(expectedVoyage, HttpStatus.ACCEPTED);
    Mockito.when(restTemplate.getForEntity(wsUrl.toString(), Voyage.class)).thenReturn(response);

    Voyage actualVoyage = voyageServiceImpl.findCurrentVoyage();

    Assert.assertEquals(expectedVoyage, actualVoyage);

    Mockito.verify(restTemplate).getForEntity(wsUrl.toString(), Voyage.class);
  }


}

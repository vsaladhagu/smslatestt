package com.princess.util;

import java.util.Arrays;
import java.util.List;

import javax.print.DocFlavor;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.PrintServiceAttributeSet;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author <a href="mailto:fvinluan@hagroup.com">Francis Vinluan</a>
 * 
 * @version $Revision: 1.0 $
 */
public final class PrintUtil {

  private static final Logger logger = LoggerFactory.getLogger(PrintUtil.class);

  /**
   * Method getPrintService.
   * 
   * @param flavor DocFlavor
   * 
   * @return PrintService
   */
  public static PrintService getPrintService(DocFlavor flavor) {
    PrintService[] services = PrintServiceLookup.lookupPrintServices(flavor, null);
    if (services.length == 0) {
      return null;
    }

    // More than one printer might be returned:
    PrintService service = services[0];
    if (logger.isInfoEnabled()) {
      logger.info("Printer " + service.getName() + " is accepting " + flavor.getMediaType());
    }
    return service;
  }


  /**
   * Method getPrintService.
   * 
   * @param printerName String
   * 
   * @return PrintService
   */
  public static PrintService getPrintService(String printerName) {
    if (logger.isDebugEnabled()) {
      logger.debug("Retrieving print service " + printerName);
    }

    PrintService[] services = PrintServiceLookup.lookupPrintServices(null, null);
    if (logger.isDebugEnabled()) {
      logger.debug("services size " + services.length);
    }
    for (PrintService service : services) {
      String serviceName = StringUtils.upperCase(service.getName());

      if (serviceName.equalsIgnoreCase(printerName)) {
        return service;
      }
    }
    return null;
  }


  /**
   * Method getPrintService.
   * 
   * @param printerName String
   * @param aset PrintServiceAttributeSet
   * 
   * @return PrintService
   */
  public static PrintService getPrintService(String printerName, PrintServiceAttributeSet aset) {
    if (logger.isDebugEnabled()) {
      logger.debug("Retrieving print service " + printerName);
    }
    PrintService[] services = PrintServiceLookup.lookupPrintServices(null, aset);
    if (logger.isDebugEnabled()) {
      logger.debug("services size " + services.length);
    }
    for (PrintService service : services) {
      String serviceName = StringUtils.upperCase(service.getName());
      logger.debug("Comparing " + printerName + " to " + serviceName);
      if (serviceName.equalsIgnoreCase(printerName)) {
        return service;
      }
    }
    return null;
  }


  /**
   * Method getPrintServices.
   * 
   * 
   * @return List<PrintService>
   */
  public static List<PrintService> getPrintServices() {
    PrintService[] services = PrintServiceLookup.lookupPrintServices(null, null);

    List<PrintService> serviceList = Arrays.asList(services);
    return serviceList;
  }


}
